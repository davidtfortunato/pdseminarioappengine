package com.test.pd;

import com.google.api.server.spi.Constant;

public class Constants {
	  public static final String WEB_CLIENT_ID = "982832232587-hbh1jse08b9ivt0gu1o50f75qjghrdg1.apps.googleusercontent.com";
	  public static final String ANDROID_CLIENT_ID = "replace this with your Android client ID";
	  public static final String IOS_CLIENT_ID = "replace this with your iOS client ID";
	  public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;
	  public static final String API_EXPLORER_CLIENT_ID = Constant.API_EXPLORER_CLIENT_ID;
	  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
	}
package com.test.pd;

import com.google.appengine.api.datastore.Entity;

public class AlunoEntity{

	public static final String ENTITY_KIND = "Aluno";
	public static final String ENTITY_PROPERTY_NOME = "nome";
	public static final String ENTITY_PROPERTY_NUMERO = "numero";
	
	private String numeroAluno;
	private String nome;
	
	
	public AlunoEntity()
	{
		nome = "Aluno Inexistente";
		numeroAluno = "Aluno Inexistente";
	}
	
	public AlunoEntity(Entity entity)
	{
		if (entity != null) {
			nome = (String) entity.getProperty(ENTITY_PROPERTY_NOME);
			numeroAluno = (String) entity.getProperty(ENTITY_PROPERTY_NUMERO);
		}
		else
		{
			nome = "Aluno Inexistente";
			numeroAluno = "Aluno Inexistente";
		}
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumeroAluno() {
		return numeroAluno;
	}

	public void setNumeroAluno(String numeroAluno) {
		this.numeroAluno = numeroAluno;
	}
	
	public Entity generateEntity()
	{
		Entity entity = new Entity(ENTITY_KIND, numeroAluno);
		entity.setProperty(ENTITY_PROPERTY_NOME, nome);
		entity.setProperty(ENTITY_PROPERTY_NUMERO, numeroAluno);
		
		return entity;
	}
}

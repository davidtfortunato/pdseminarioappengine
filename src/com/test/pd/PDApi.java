package com.test.pd;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.mortbay.log.Log;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.tools.util.Logging;

@Api(name = "pdapi", version = "v2", clientIds = { Constants.WEB_CLIENT_ID,
		Constants.API_EXPLORER_CLIENT_ID }, description = "API para demonstracao da cadeira de PD")
public class PDApi {

	@ApiMethod(name = "getAlunoDefault", path = "getAlunoDefault", httpMethod = HttpMethod.GET)
	public AlunoEntity getAlunoDefault() {
		AlunoEntity alunoEntity = new AlunoEntity();
		alunoEntity.setNome("Aluno Default");
		alunoEntity.setNumeroAluno("123456");

		return alunoEntity;
	}

	@ApiMethod(name = "getAlunoByNumero", path = "getAlunoByNumero", httpMethod = HttpMethod.GET)
	public AlunoEntity getAlunoByNumero(@Named("number") String number) {

		Entity entity = null;

		try {
			entity = DatastoreServiceFactory.getDatastoreService()
					.get(KeyFactory.createKey(AlunoEntity.ENTITY_KIND, number));

		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AlunoEntity alunoEntity = new AlunoEntity(entity);

		return alunoEntity;
	}

	@ApiMethod(name = "getAlunoByNome", path = "getAlunoByNome", httpMethod = HttpMethod.GET)
	public AlunoEntity getAlunoByNome(@Named("nome") String nome) {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(AlunoEntity.ENTITY_KIND)
				.setFilter(new FilterPredicate(AlunoEntity.ENTITY_PROPERTY_NOME, FilterOperator.EQUAL, nome));

		PreparedQuery pq = datastore.prepare(q);
		Entity entity = pq.asSingleEntity();
		AlunoEntity alunoEntity = new AlunoEntity(entity);

		return alunoEntity;
	}

	@ApiMethod(name = "getAlunos", path = "getAlunos", httpMethod = HttpMethod.GET)
	public List<AlunoEntity> getAlunos() {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(AlunoEntity.ENTITY_KIND).addSort(AlunoEntity.ENTITY_PROPERTY_NUMERO,
				SortDirection.DESCENDING);

		PreparedQuery pq = datastore.prepare(q);
		List<AlunoEntity> listAlunos = new ArrayList<>();

		Iterator<Entity> itEntities = pq.asIterator();
		while (itEntities.hasNext()) {
			listAlunos.add(new AlunoEntity(itEntities.next()));
		}

		return listAlunos;
	}

	@ApiMethod(name = "postAluno", path = "postAluno", httpMethod = HttpMethod.POST)
	public AlunoEntity postAluno(@Named("name") String name, @Named("number") String number) {
		AlunoEntity alunoEntity = new AlunoEntity();
		alunoEntity.setNome(name);
		alunoEntity.setNumeroAluno(number);

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		datastore.put(alunoEntity.generateEntity());

		return alunoEntity;
	}

	@ApiMethod(name = "deleteAlunoByNumero", path = "deleteAlunoByNumero", httpMethod = HttpMethod.DELETE)
	public AlunoEntity deleteAlunoByNumero(@Named("number") String number) {

		try {
			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

			Key key = KeyFactory.createKey(AlunoEntity.ENTITY_KIND, number);
			Entity alunoEntity = datastore.get(key);

			if (alunoEntity != null) {
				datastore.delete(KeyFactory.createKey(AlunoEntity.ENTITY_KIND, number));
				return new AlunoEntity(alunoEntity);
			}
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new AlunoEntity();
	}

}
